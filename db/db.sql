create database clients;

use clients;

create table client (
  id int primary key auto_increment,
  fname varchar(50) not null,
  sname varchar(100) not null
);

insert into client (fname, sname) values ('Jouni','Juntunen');
