<?php require_once 'inc/top.php'; ?>
<h1>Clients</h1>
<?php
$sql = 'select * from client order by sname';
$query = $db->query($sql);
$query->setFetchMode(PDO::FETCH_OBJ);
if ($query) 
{
?>
<table>
  <tr>
    <th>Client id</th>
    <th>First name</th>
    <th>Last name</th>
    <th></th>
    <th></th>
  </tr>
  <?php
  while ($record = $query->fetch()) {
    print '<tr>';
    print "<td>$record->id</td>";
    print "<td>$record->fname</td>";
    print "<td>$record->sname</td>";
    print "<td><a href='delete.php?id=$record->id'>Delete</a></td>";
    print "<td><a href='client.php?id=$record->id'>Edit</a></td>";
    print '</tr>';
  }
  ?>
<?php 
} 
else 
{
  print '<p>Error retrieving client information!</p>';
}
?>
</table>
<?php require_once 'inc/bottom.php'; ?>