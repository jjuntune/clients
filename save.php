<?php
require_once 'inc/top.php';

$id = filter_input(INPUT_POST,'id',FILTER_SANITIZE_NUMBER_INT);
$fname = filter_input(INPUT_POST,'fname',FILTER_SANITIZE_STRING);
$lname = filter_input(INPUT_POST,'lname',FILTER_SANITIZE_STRING);

if ($id == 0) {
  $query = $db->prepare("insert into client (fname, sname) values (:fname,:lname)");
}
else {
  $query = $db->prepare("update client set fname=:fname,sname=:lname where id=:id");
  $query->bindValue(':id',$id,PDO::PARAM_INT);  
}

$query->bindValue(':fname',$fname,PDO::PARAM_STR);
$query->bindValue(':lname',$lname,PDO::PARAM_STR);

if ($query->execute()) 
{
  print '<p>Data saved!</p>';
}
else {
  print '<p>Error saving client</p>';
}
print '<a href="index.php">Browse clients</a>';
require_once 'inc/bottom.php';
?>