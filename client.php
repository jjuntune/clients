<?php require_once 'inc/top.php'; ?>
<?php
$id = 0;
$fname = '';
$lname = '';
if (isset($_GET['id'])) {
  $id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_NUMBER_INT);
  $query = $db->query("select * from client where id = $id");
  if ($query) {
    $record = $query->fetch();
    $fname = $record['fname'];
    $lname = $record['sname'];
  }
}
?>
<h1>Add client</h1>
<form action="save.php" method="post">
  <input type="hidden" value="<?php print($id);?>" name="id">
  <div class="form-group">
    <label for="fname">First name</label>
    <input value="<?php print($fname);?>" type="text" class="form-control" name="fname" placeholder="Enter first name">
  </div>
  <div class="form-group">
    <label for="lname">Last name</label>
    <input value="<?php print($lname);?> "type="text" class="form-control" name="lname" placeholder="Enter last name">
  </div>
  <button type="submit" class="btn btn-primary">Save</button>
</form
<?php require_once 'inc/bottom.php'; ?>