<?php require_once 'inc/top.php';?>
<h1>Delete client</h1>
<?php
$id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_NUMBER_INT);
$query = $db->prepare("delete from client where id = :id");

$query->bindValue(':id',$id,PDO::PARAM_INT);

if ($query->execute()) 
{
  print '<p>Client deleted!</p>';
}
else {
  print '<p>Error deleting client</p>';
}
print '<a href="index.php">Browse clients</a>';
?>
<?php require_once 'inc/bottom.php';?>